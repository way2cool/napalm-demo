import napalm

driver = napalm.get_network_driver('ios')

optional_args = {'secret': 'cisco'}

device = driver('192.168.1.151', 'cisco', 'cisco', optional_args=optional_args)
device.open()


conf = device.get_config()
print(conf['running'])


device.close()
